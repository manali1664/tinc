<?php 
	error_reporting(E_ALL & ~E_NOTICE);
	$conn = mysqli_connect("localhost","root","","demodev_crm");
	$result = mysqli_query($conn,"CALL capitation_check()") or die("query fails:" .mysqli_error());
	$json_data = array();
	$currDate = date('Y-m-d');
			

while($row = mysqli_fetch_array($result))
	{
			$reporting_entity_name = $row['reporting_entity_name'];
			$reporting_entity_type = $row['reporting_entity_type'];
			
			$billing_name = explode("##", $row['name']);
			$billing_code_type = explode("##", $row['billing_code_type']);
			$billing_code_type_version = explode("##", $row['billing_code_type_version']);
			$service_code = explode("##", $row['service_code']);
			$billing_code = explode("##", $row['billing_code']);
			$description = explode("##", $row['description']);
			$negotiation_arrangement = explode("##", $row['negotiation_arrangement']);
			$negotiated_rate = explode("##", $row['negotiated_rate']);
			$tin = explode("##", $row['tin']);
			$providerNpi = explode("##", $row['providerNpi']);
			$expiration_date = explode("##", $row['expiration_date']);
			$name_IIN = explode("##", $row['name_IIN']);
			$billing_code_type_IIN = explode("##", $row['billing_code_type_IIN']);
			$billing_code_type_version_IIN = explode("##", $row['billing_code_type_version_IIN']);
			$billing_code_IIN = explode("##", $row['billing_code_IIN']);
			$description_IIN = explode("##", $row['description_IIN']);
			
			$billing_data = array();
			foreach($billing_name as $key =>$value)
			{
				$billing_name_1 = $value;
				$billing_code_type_1 = $billing_code_type[$key];
				$billing_code_type_version_1 = $billing_code_type_version[$key];
				$service_code_1 = $service_code[$key];
				$billing_code_1 = $billing_code[$key];
				$description_1 = $description[$key];
				$negotiation_arrangement_1 = $negotiation_arrangement[$key];
				$negotiated_rate_1 = $negotiated_rate[$key];
				$tin_1 = $tin[$key];
				$providerNpi_1 = $providerNpi[$key];
				$expiration_date_1 = $expiration_date[$key];
				$name_IIN_1 = $name_IIN[$key];
				$billing_code_type_IIN_1 = $billing_code_type_IIN[$key];
				$billing_code_type_version_IIN_1 = $billing_code_type_version_IIN[$key];
				$billing_code_IIN_1 = $billing_code_IIN[$key];
				$description_IIN_1 = $description_IIN[$key];
			
				$negotiated_rate_2 = explode("~~", $negotiated_rate_1);
				$tin_2 = explode("~~", $tin_1);
				$providerNpi_2 = explode("~~", $providerNpi_1);
				
				$negotiated_val = array();
				foreach($negotiated_rate_2 as $key => $value){
					$negotiated_rate_3 = $value;
					$tin_3 = $tin_2[$key];
					$providerNpi_3 = $providerNpi_2[$key];
					
					$tin_4 = explode("::",$tin_3);
					$providerNpi_4 = explode("::",$providerNpi_3);
					$providers = array();
					foreach($tin_4 as $key=>$value){
						$tin_5 = $value;
						$providerNpi_5 = $providerNpi_4[$key];
						$providers[]=array(
							'tin'=>$tin_5,
							'service_code' => $service_code_1,
							'npi' =>$providerNpi_5
							);
					}
					
					$negotiated_val[]=array(
						'negotiated_rate'=> $negotiated_rate_3,
						'providers' =>$providers
					);
				}
				$bundle_code = array();
					$name_IIN_2 = explode("::", $name_IIN_1);
					$billing_code_type_IIN_2 = explode("::", $billing_code_type_IIN_1);
					$billing_code_type_version_IIN_2 = explode("::", $billing_code_type_version_IIN_1);
					$billing_code_IIN_2 = explode("::", $billing_code_IIN_1);
					$description_IIN_2 = explode("::", $description_IIN_1);
					
						foreach($name_IIN_2 as $key=>$value){
						$name_IIN_3 = $value;//print_r($name_IIN_3); echo '===';
						$billing_code_type_IIN_3 = $billing_code_type_IIN_2[$key];
						$billing_code_type_version_IIN_3 = $billing_code_type_version_IIN_2[$key];
						$billing_code_IIN_3 = $billing_code_IIN_2[$key];
						$description_IIN_3 = $description_IIN_2[$key];
						
						$name_IIN_4 = explode("**", $name_IIN_3);
						$billing_code_type_IIN_4 = explode("**", $billing_code_type_IIN_3);
						$billing_code_type_version_IIN_4 = explode("**", $billing_code_type_version_IIN_3);
						$billing_code_IIN_4 = explode("**", $billing_code_IIN_3);
						$description_IIN_4 = explode("**", $description_IIN_3);
						
							foreach($name_IIN_4 as $key =>$value){
								$name_IIN_5 = $value; 
								//echo $name_IIN_5.'=========';
								if($name_IIN_5!=''){
									$billing_code_type_IIN_5 = $billing_code_type_IIN_4[$key];
									$billing_code_type_version_IIN_5 = $billing_code_type_version_IIN_4[$key];
									$billing_code_IIN_5 = $billing_code_IIN_4[$key];
									$description_IIN_5 = $description_IIN_4[$key];
									
										$bundle_code[] = array(
										  'billing_code_type' => $billing_code_type_IIN_5,
										  'billing_code_type_version' => $billing_code_type_version_IIN_5,
										  'billing_code' => $billing_code_IIN_5,
										  'description' =>  $description_IIN_5
									);
								}
							}
						}
				
				//print_r(json_encode($bundle_code));
				$billing_data[] = array(
				'negotiation_arrangement' => $negotiation_arrangement_1,
				'name' => utf8_encode($billing_name_1),
				'description'=> $description_1,
				'negotiated_rates' => $negotiated_val,
				'covered_services' => $bundle_code
				);
				
			}
			
			$json_data[] = array(
				'plan_name' => $row['plan_name'],
				'plan_id_type' => $row['plan_id_type'],
				'plan_id' => $row['plan_id'],
				'plan_market_type' => $row['plan_market_type'],
				'last_updated_on' => $currDate,
				'in_network' => $billing_data
				);
				
	}
	$capitation_json = array(
				'reporting_entity_name'=>$reporting_entity_name,
				'reporting_entity_type'=>$reporting_entity_type,
				'plan'=>$json_data
				);
		
		echo json_encode($capitation_json,true);

?>