<?php 

	$conn = mysqli_connect("localhost","root","","demodev_crm");
	$result = mysqli_query($conn,"CALL OON_tinC()") or die("query fails:" .mysqli_error());
	$json_data = array();
	$bill_charge = array();
	
			$plan_name = 'tinC poc'; // constant
			$plan_id = '12345XX9876543'; // constant
			$plan_id_type = 'HIOS'; // constant
			$plan_market_type = 'Individual'; // constant
			$reporting_entity_name = "medicare"; // constant
			$reporting_entity_type = "medicare"; // constant
			$currDate = date('Y-m-d');

while($row = mysqli_fetch_array($result))
	{
		//print_r($row);
		$totalCharges = explode("::", $row['totalCharges']);
		
		$providerNPI = explode("::", $row['providerNPI']);
		foreach($totalCharges as $key => $value){
			$check_charge = $value;
			$check_npi = $providerNPI[$key];
			//print_r($check_npi);
				$bill_charge[] = array(
				'billed_amount'=> $check_charge,
				'npi' => $check_npi
			);
		}
		$json_data[] = array(
				'name' => $row['billing_name'],
				'billing_code_type' => 'CPT',
				'billing_code_type_version' => '2021',
				'billing_code' => $row['procedureCode'],
				'description'=> $row['description'],
				'allowed_amounts' => array(
					'tin'=> $row['providerTIN'],
					'service_Code'=> $row['placeOfService'],
					'payments' => array(
							'allowed_amount' => $row['allowedAmount'],
							/*'providers' =>array(
								'billed_amount' => $totalCharges,
								'npi' => $row['providerNPI']
								)*/
							  'providers'=> $bill_charge
						)
				)
		);
		$bill_charge = array();
		$totalCharges = '';
		$providerNPI = '';
	}
	
	
		$finalJson = array(
			'reporting_entity_name'=>$reporting_entity_name,
			'reporting_entity_type'=>$reporting_entity_type,
			'plan_name'=>$plan_name,
			'plan_id'=>$plan_id,
			'plan_id_type'=>$plan_id_type,
			'plan_market_type' => $plan_market_type,
			'last_updated_on' => $currDate,
			'out_of_network' =>$json_data
			);
			
echo json_encode($finalJson, JSON_PRETTY_PRINT);

?>