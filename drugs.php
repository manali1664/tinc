<?php
	error_reporting(E_ALL & ~E_NOTICE);
	$conn = mysqli_connect("localhost","root","","demodev_crm");
	$result = mysqli_query($conn,"CALL prescription_drug()") or die("query fails:" .mysqli_error());
	$json_data = array();
	$currDate = date('Y-m-d');
	
	while($row = mysqli_fetch_array($result))
	{
		
		$reporting_entity_name = $row['reporting_entity_name'];
		$reporting_entity_type = $row['reporting_entity_type'];
		
		$ndc = explode("##", $row['ndc']);
		$drug_type = explode("##", $row['drug_type']);
		$drug_name = explode("##", $row['drug_name']);
		$historical_net_price = explode("##", $row['historical_net_price']);
		$historical_net_reporting_period = explode("##", $row['historical_net_reporting_period']);
		$negotiated_rate = explode("##", $row['negotiated_rate']);
		$administrative_fee = explode("##", $row['administrative_fee']);
		$dispensing_fee = explode("##", $row['dispensing_fee']);
		$transaction_fee = explode("##", $row['transaction_fee']);
		$tin = explode("##", $row['tin']);
		$service_code = explode("##", $row['service_code']);
		$npi = explode("##", $row['npi']);
		$pharmacy_id_type = explode("##", $row['pharmacy_id_type']);
		$pharmacy_ids = explode("##", $row['pharmacy_ids']);
		
		$drugs = array();
		foreach($ndc as $key => $value){
			$ndc_1 = $value;
			$drug_type_1 = $drug_type[$key];
			$drug_name_1 = $drug_name[$key];
			$historical_net_price_1 = $historical_net_price[$key];
			$historical_net_reporting_period_1 = $historical_net_reporting_period[$key];
			$negotiated_rate_1 = $negotiated_rate[$key];
			$administrative_fee_1 = $administrative_fee[$key];
			$dispensing_fee_1 = $dispensing_fee[$key];
			$transaction_fee_1 = $transaction_fee[$key];
			$tin_1 = $tin[$key];
			$service_code_1 = $service_code[$key];
			$npi_1 = $npi[$key];
			$pharmacy_id_type_1 = $pharmacy_id_type[$key];
			$pharmacy_ids_1 = $pharmacy_ids[$key];
			
			$historical_net_price_2 = explode("~~", $historical_net_price_1);
			$historical_net_reporting_period_2 = explode("~~", $historical_net_reporting_period_1);
			$negotiated_rate_2 = explode("~~", $negotiated_rate_1);
			$administrative_fee_2 = explode("~~", $administrative_fee_1);
			$dispensing_fee_2 = explode("~~", $dispensing_fee_1);
			$transaction_fee_2 = explode("~~", $transaction_fee_1);
			$tin_2 = explode("~~", $tin_1);
			$service_code_2 = explode("~~", $service_code_1);
			$npi_2 = explode("~~", $npi_1);
			$pharmacy_id_type_2 = explode("~~", $pharmacy_id_type_1);
			$pharmacy_ids_2 = explode("~~", $pharmacy_ids_1);
			
			$prices = array();
			foreach($historical_net_price_2 as $key =>$value){
				$historical_net_price_3 = $value;
				$historical_net_reporting_period_3 = $historical_net_reporting_period_2[$key];
				$negotiated_rate_3 = $negotiated_rate_2[$key];
				$administrative_fee_3 = $administrative_fee_2[$key];
				$dispensing_fee_3 = $dispensing_fee_2[$key];
				$transaction_fee_3 = $transaction_fee_2[$key];
				$tin_3 = $tin_2[$key];
				$service_code_3 = $service_code_2[$key];
				$npi_3 = $npi_2[$key];
				$pharmacy_id_type_3 = $pharmacy_id_type_2[$key];
				$pharmacy_ids_3 = $pharmacy_ids_2[$key];
				
				$pharmacy_id_type_4 = explode("::", $pharmacy_id_type_3);
				$pharmacy_ids_4 = explode("::", $pharmacy_ids_3);
				$pharmacies = array();
				foreach($pharmacy_id_type_4 as $key => $value){
					$pharmacy_id_type_5 = $value;
					$pharmacy_ids_5 = $pharmacy_ids_4[$key];
					
					$pharmacies[]= array(
						'pharmacy_id_type'=> $pharmacy_id_type_5,
						'pharmacy_ids'=> $pharmacy_ids_5
					);
				}
				//print_r(json_encode($pharmacies));
				$prices[]= array(
					'historical_net_price'=> $historical_net_price_3,
					'historical_net_reporting_period'=> $historical_net_reporting_period_3,
					'negotiated_rate'=> $negotiated_rate_3,
					'administrative_fee' => $administrative_fee_3,
					'dispensing_fee' => $dispensing_fee_3,
					'transaction_fee'=> $transaction_fee_3,
					'tin'=>$tin_3,
					'service_code'=> $service_code_3,
					'npi'=> $npi_3,
					'pharmacies'=>$pharmacies
					
				);
			}
			//print_r(json_encode($prices));
			$drugs[] = array(
				'drug_name'=> $drug_name_1,
				'drug_type'=> $drug_type_1,
				'ndc'=> $ndc_1,
				'prices'=>$prices
			);
		}
		//print_r(json_encode($drugs));
		$json_data[] = array(
				'plan_name' => $row['plan_name'],
				'plan_id_type' => $row['plan_id_type'],
				'plan_id' => $row['plan_id'],
				'plan_market_type' => $row['plan_market_type'],
				'last_updated_on' => $currDate,
				'drugs' => $drugs
				);
	}
		$drugs_json = array(
				'reporting_entity_name'=>$reporting_entity_name,
				'reporting_entity_type'=>$reporting_entity_type,
				'plan'=>$json_data
				);
	print_r(json_encode($drugs_json));
	//echo json_encode($drugs_json,true);
?>